<?php

namespace IdvBundle\Controller;


use IdvBundle\Entity\Categorie;
use IdvBundle\Entity\Demande;
use IdvBundle\Entity\Group;
use IdvBundle\Entity\Image;
use IdvBundle\Entity\Requester;
use IdvBundle\Entity\Utilisateur;
use IdvBundle\Helper\AnnotateHelper;
use IdvBundle\Models\Document;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class IdvController extends Controller
{
    protected $valideTypes = ['jpg', 'jpeg', 'bmp', 'png', 'gif', 'dcm'];
    /**
     * @Route("/", name="home", options={"expose"=true})
     */
    public function indexAction(Request $request)
    {
        return $this->render('IdvBundle:Idv:index.html.twig');
    }

    /**
     * @Route("/signup", name="sign_up")
     */
    public function signupAction(Request $request)
    {

        $user = null;
        $url = "";
        if($request->isMethod("post") && ($request->request->get("nom")) && ($request->request->get("prenom")) && ($request->request->get("email")) && ($request->request->get("password")) && $request->request->get("idv") == "idv" && $request->request->get("conditions")){
            if($request->request->get("type") == "requester"){
                $user = new Utilisateur();
            }
            else if($request->request->get("type") == "crowder"){
                $user = new Requester();
            }
            else
                return  $this->redirect($this->render("home"));

            $username = $request->request->get("nom")." ".$request->request->get("prenom");

            $user->setNom($request->request->get("nom"));
            $user->setPrenom($request->request->get("prenom"));
            $user->setEmail($request->request->get("email"));
            $user->setUsername($username);
            $user->setDescription("");
            $user->setPassword($request->request->get("password"));
            if($user instanceof Requester)
                $user->setReputation(50);

            if($request->files->get("profile")){
                $file = $request->files->get("profile");
                $status = 'success';
                $uploadedURL='';
                $message='';
                if(($file instanceof UploadedFile) && ($file->getError() == 0)){

                    if (($file->getSize() < 2000000000)) {
                        $originalName = $file->getClientOriginalName();
                        $name_array = explode('.', $originalName);
                        $file_type = $name_array[sizeof($name_array) - 1];
                        $valid_filetypes = array('jpg', 'jpeg', 'bmp', 'png', 'gif');
                        if (in_array(strtolower($file_type), $valid_filetypes)) {
                            //Start Uploading File

                            $document = new Document();
                            $document->setFile($file);
                            $document->setSubDirectory('profiles');
                            $document->processFile();
                            $uploadedURL = '/uploads/'. $document->getSubDirectory() .
                                '/' . $file->getBasename();
                            $user->setUrl($uploadedURL);
                        } else {
                            $status = 'failed';
                            $message = 'Invalid File Type';
                        }
                    } else {
                        $status = 'failed';
                        $message = 'Size exceeds limit';
                    }
                }
            }else

                $user->setUrl('/bundles/idv/images/no-image.png');


            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $msg = \Swift_Message::newInstance()
                ->setSubject("Confirmation d'enregistrement")
                ->setFrom('idvparisdescartes@gmail.com')
                ->setTo($user->getEmail())
                ->setBody(
                    $this->renderView(
                    // app/Resources/views/Emails/registration.html.twig
                        'IdvBundle:Idv:registration.html.twig',
                        array('name' => $user->getNom())
                    ),
                    'text/html'
                )
            ;
            $this->get('mailer')->send($msg);

            if(!$request->getSession()->isStarted())
                $request->getSession()->start();

            if($user instanceof Utilisateur){

                $request->getSession()->set("requesterSession", $user);
                $url = "requester_profile";
            }
            else if($user instanceof Requester){

                $request->getSession()->set("crowderSession", $user);
                $url = "crowder_profile";
            }

            return $this->redirect($this->generateUrl($url));
        }
        return$this->redirect($this->generateUrl("home"));
    }

    /**
     * @Route("/signin", name="sign_in")
     */
    public function singinAction()
    {

        return $this->render('IdvBundle:Idv:signin.html.twig');
    }

    /**
     * @Route("/user/forgot_password", name="idv_forgot_password",  options={"expose"=true})
     */
    public function forgotPasswordAction(Request $request)
    {
        $type = $request->get('type');
        $email = $request->get('email');
        $user = null;
        // if corwder
        if($type == 'requester'){
            $rep = $this->getDoctrine()->getRepository("IdvBundle:Utilisateur");
            $user = $rep->findOneBy(array('email'=> $email));
        // if requester
        } elseif($type == 'crowder'){
            $rep = $this->getDoctrine()->getRepository("IdvBundle:Requester");
            $user = $rep->findOneBy(array("email"=>$email));
        }
        if($user){
            $password = $user->getPassword();
            $this->sendForgetPassword($email, $password);
            $result = ['success' => true, 'message' => 'mot de passe envoyé à '.$email];
        } else {
            $result = ['success' => false, 'message' => 'Aucun compte ne correspond à cette adresse mail'];
        }

        return new JsonResponse($result);
    }

    /**
     * @Route("/requester/login", name="requester_login")
     */
    public function loginAction(Request $request)
    {
        if(!$request->getSession()->get("requesterSession")){
            if($request->isMethod('post')){
                $email = $request->request->get("email");
                $pass = $request->request->get("password");
                if(!empty($email) && !empty($pass)){

                    $rep = $this->getDoctrine()->getRepository("IdvBundle:Utilisateur");
                    $user = $rep->findOneBy(array('email'=> $email, 'password'=> $pass));

                    if($user) {
                        $request->getSession()->set("requesterSession", $user);
                        $url = $this->generateUrl("requester_profile");
                        return $this->redirect($url);
                    }
                }
            }
        }else
            return $this->redirect($this->generateUrl("requester_profile"));

        return $this->redirect($this->generateUrl("sign_in"));
    }

    /**
     * @Route("/requester/profile", name="requester_profile")
     */
    public function showProfileAction(Request $request)
    {

        $popup = $request->get('popup');
        $popup = ($popup)? $popup: 'hide';
        if($request->getSession()->get("requesterSession")){

            $em = $this->getDoctrine()->getManager();
            $rep = $em->getRepository("IdvBundle:Utilisateur");
            $rep_cat = $em->getRepository("IdvBundle:Categorie");

            $user = $user = $rep->find($request->getSession()->get("requesterSession")->getId());
            $cat = $rep_cat->findAll();
            return $this->render('IdvBundle:Idv:profil.html.twig', array('user'=> $user, "category"=>$cat, 'popup' => $popup));
        }
        $url = $this->generateUrl("sign_in");
        return $this->redirect($url);
    }

    /**
     * @Route("/requester/profile/edit", name="requester_profile_edit")
     */
    public function editProfileAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        if($request->isMethod("POST") && $request->getSession()->get("requesterSession")){

            $rep = $this->getDoctrine()->getRepository("IdvBundle:Utilisateur");
            $user = $rep->find($request->getSession()->get("requesterSession")->getId());

            if($request->request->get("opassword") == $user->getPassword()) {
                $user->setNom($request->request->get("nom"));
                $user->setPrenom($request->request->get("prenom"));
                $user->setEmail($request->request->get("email"));
                $user->setusername($request->request->get("username"));
                $user->setDescription($request->request->get("description"));

                $postedPassword = $request->request->get("cpassword");
                if($request->request->get("npassword") == $postedPassword){
                    if($postedPassword) {
                        $user->setPassword($request->get("npassword"));
                    }

                }
                // upload the file
                if($request->files->get("profile")){
                    $file = $request->files->get("profile");
                    $status = 'success';
                    $uploadedURL='';
                    $message='';
                    if(($file instanceof UploadedFile) && ($file->getError() == 0)){
                        if (($file->getSize() < 2000000000)) {
                            $originalName = $file->getClientOriginalName();
                            $name_array = explode('.', $originalName);
                            $file_type = $name_array[sizeof($name_array) - 1];
                            $valid_filetypes = array('jpg', 'jpeg', 'bmp', 'png');
                            if (in_array(strtolower($file_type), $valid_filetypes)) {
                                //Start Uploading File

                                $document = new Document();
                                $document->setFile($file);
				                $document->setUploadDirectory($this->getParameter('kernel.root_dir').'/../web/uploads');
                                $document->setSubDirectory('profiles');
                                $document->processFile();
                                $uploadedURL = '/uploads/'. $document->getSubDirectory() .
                                    '/' . $file->getBasename();

                                $user->setUrl($uploadedURL);
                            } else {
                                $status = 'failed';
                                $message = 'Invalid File Type';
                            }
                        } else {
                            $status = 'failed';
                            $message = 'Size exceeds limit';
                        }
                    }
                }

                $em->flush();
                $request->getSession()->set("requesterSession", $user);
            }
        }

        return $this->redirect($this->generateUrl("requester_profile"));
    }

    /**
     *
     * @Route("/requester/request/create", name="request_create")
     */
    public function createRequestAction(Request $request)
    {
        $popCreation ='hide';
        if ($request->getSession()->get("requesterSession")){
            if ($request->isMethod("post")) {
                $popCreation = 'show';
                $em = $this->getDoctrine()->getManager();
                $rep_owner = $em->getRepository("IdvBundle:Utilisateur");
//                $rep_category = $em->getRepository("IdvBundle:Categorie");

                $owner = $rep_owner->find($request->getSession()->get("requesterSession")->getId());
//                $cat = $rep_category->find($request->request->get("categrie"));

                $demande = new Demande();
                $demande->setTitre($request->request->get("titre"));
                $demande->setDescription($request->request->get("description"));
                $demande->setDatePublication(new \DateTime('now'));
                $date = $request->request->get("aaaa")."-".$request->request->get("mm")."-".$request->request->get("jj");
                $demande->setEcheance(new \DateTime($date));
                $demande->setVerouiller(1);
                $demande->setType(Demande::TYPE_SEGMENTATION);
//                $demande->setCategorie($cat);
                $demande->setOwner($owner);
                $demande->setCode($this->getRandomString(5));

                $demande = $this->uploadFile($request, $demande);

                $em->persist($demande);
                $em->flush();
            }
        }

        return $this->redirect($this->generateUrl("requester_profile", array('popup' => $popCreation)));

    }

    public function uploadFile(Request $request, Demande $demande)
    {

        include __DIR__.'/../External/class_dicom.php';
        $em = $this->getDoctrine()->getManager();
        $index = (int) $request->request->get("group_index");

        for ($j = 1; $j <= $index; $j++) {
            $group = new Group();
            $em->persist($group);

            $file = $request->files->get("img_".$j);
            $status = 'success';
            $uploadedURL='';
            $message='';
            $file_count=count($file);
            for ($i=0; $i<$file_count; $i++) {
                $image = new Image();
                if (($file[$i] instanceof UploadedFile) && ($file[$i]->getError() == 0)) {

                    if (($file[$i]->getSize() < 2000000000)) {
                        $originalName = $file[$i]->getClientOriginalName();
                        $fileInfos = pathinfo($originalName);
                        $file_type = $fileInfos['extension'];
                        if (in_array(strtolower($file_type), $this->valideTypes)) {
                            //Start Uploading File
                            $document = new Document();
                            $document->setFile($file[$i]);
                            $document->setUploadDirectory($this->getParameter('kernel.root_dir') . '/../web/uploads');
                            $document->setSubDirectory('uploads');
                            $document->processFile();
                            $uploadedURL = '/uploads/' . $document->getSubDirectory() . '/' . $file[$i]->getBasename();
                            $fileFullPath = $this->getParameter('kernel.root_dir') . '/../web/uploads/uploads/'.$file[$i]->getBasename();
                            if($file_type =='dcm') {
                                $d = new \dicom_convert;
                                $d->file = $fileFullPath;
                                $d->dcm_to_jpg();
                                // delete the tmp file
                                \unlink($fileFullPath);
                                $uploadedURL.='.jpg';
                            }
                            $demande->setUrl($uploadedURL);
                            $image->setUrl($uploadedURL);
                            $image->setDemande($demande);
                            $image->setGroup($group);
                            $demande->addDemandeImage($image);
                        } else {
	                        $status = 'failed';
	                        $message = 'Invalid File Type';
	                        die($message);
                        }
                    } else {
                        $status = 'failed';
                        $message = 'Size exceeds limit';
                        die($message);
                    }
                } else {
                    die('Erreur');
                }

            }
        }

        return $demande;

    }


    /**
     * @Route("/requester/request/edit")
     */
    public function editRequest(Request $request){

        if($request->isMethod("post") && $request->getSession()->get("requesterSession")){
            $em = $this->getDoctrine()->getManager();
            $rep = $em->getRepository("IdvBundle:Demande");
            $d = $rep->find($request->request->get("id"));
            if($d){

                $d->setVerouiller($request->request->get("lock"));
                $em->flush();
                return new Response("done");
            }
        }
        return new Response('Error', 500);
    }

    /**
     *
     * @Route("/requester/request/{id_r}", name="request_show")
     */
    public function showRequestAction($id_r, Request $request){

        if ($request->getSession()->get("requesterSession")){
            $em = $this->getDoctrine()->getManager();
            $rep = $em->getRepository("IdvBundle:Demande");
            $demande = $rep->find($id_r);
            /** @var Image $image */
            $image = $demande->getDemandeImages()->first();

            if($demande->getType() == Demande::TYPE_ANNOTATION)
                return $this->redirect($this->generateUrl("requester_show_image_annotation",  ["image" => $image->getId()]));
            if($demande->getType() == Demande::TYPE_SEGMENTATION) {
                $group = $image->getGroup();
                return $this->redirect($this->generateUrl("requester_show_image",  ["image" => $image->getId(), "group" => $group->getId()]));
            }
            if($demande->getType() == Demande::TYPE_BIOLOGISTE)
                return $this->redirect($this->generateUrl("requester_show_image_biologiste",  ["image" => $image->getId()]));
        }
        return $this->redirect($this->generateUrl("sign_in"));
    }

    /**
     * @Route("/requester/request/delete/{id}", name="request_delete")
     */
    public function deleteRequestAction(Request $request, $id){

        if ($request->getSession()->get("requesterSession")){
            $em = $this->getDoctrine()->getManager();
            $rep = $em->getRepository("IdvBundle:Demande");
            $demande = $rep->find($id);

            $em->remove($demande);
            $em->flush();

            return $this->redirect($this->generateUrl("requester_profile"));
        }
        return $this->redirect($this->generateUrl("sign_in"));
    }

    /**
     *
     * @Route("/requester/logout", name="logout_requester")
     */
    public function deconnAction(Request $req)
    {
        $req->getSession()->remove("requesterSession");
        $req->getSession()->remove("crowderSession");
        $req->getSession()->clear();
        return $this->redirect($this->generateUrl('home'));
    }

    public function getRandomString($length = 5) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $string = '';

        for ($i = 0; $i < $length; $i++) {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }

        return $string;
    }


    /**
     * @Route("/requester/request/group/{group}/image/{image}", name="requester_show_image")
     * @param Request $request
     * @param Image $image
     * @return Response
     */
    public function imageRequestShowAction(Request $request, Image $image, Group $group)
    {
        /** @var AnnotateHelper $annotateHelper */
        $annotateHelper = $this->get('annotation_helper');

        $annotations = $annotateHelper->getRepository('IdvBundle:Annotation')->findBy(['imageAnnotation' => $image]);
        return $this->render("IdvBundle:Idv:request.html.twig", array("currentImage"=> $image, 'annotations' => $annotations, 'group' => $group));

    }

    /**
     * @Route("/requester/request/annotation/image/{image}", name="requester_show_image_annotation")
     * @param Request $request
     * @param Image $image
     * @return Response
     */
    public function imageRequestShowAnnotationAction(Request $request, Image $image)
    {
        /** @var AnnotateHelper $annotateHelper */
        $annotateHelper = $this->get('annotation_helper');

        $annotations = $annotateHelper->getRepository('IdvBundle:Annotation')->findBy(['imageAnnotation' => $image]);
        return $this->render("IdvBundle:Idv:request_annotation.html.twig", array("currentImage"=> $image, 'annotations' => $annotations));

    }
    /**
     * @todo do stuff herer abdé
     * @Route("/requester/request/annotation_biologiste/image/{image}", name="requester_show_image_biologiste")
     * @param Request $request
     * @param Image $image
     * @return Response
     */
    public function imageRequestShowAnnotationBiologisteAction(Request $request, Image $image)
    {
        return $this->render("IdvBundle:Idv:request_biologiste.html.twig", array("currentImage"=> $image));
    }

    /**
     * @Route("/requester/request/annotation/create", name="request_annotation_create")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function createRequestAnnotationAction(Request $request)
    {
        include __DIR__.'/../External/class_dicom.php';

        $popCreation ='hide';
        if ($request->getSession()->get("requesterSession")){
            if ($request->isMethod("post")) {
                $popCreation = 'show';
                $em = $this->getDoctrine()->getManager();
                $rep_owner = $em->getRepository("IdvBundle:Utilisateur");

                $owner = $rep_owner->find($request->getSession()->get("requesterSession")->getId());

                $demande = new Demande();
                $demande->setTitre($request->request->get("titre"));
                $demande->setDescription($request->request->get("description"));
                $demande->setDatePublication(new \DateTime('now'));
                $date = $request->request->get("aaaa")."-".$request->request->get("mm")."-".$request->request->get("jj");
                $demande->setEcheance(new \DateTime($date));
                $demande->setVerouiller(1);
                $demande->settype(demande::TYPE_ANNOTATION);
                $demande->setOwner($owner);
                $demande->setCode($this->getRandomString(5));


                $file = $request->files->get("img");
                $status = 'success';
                $uploadedURL='';
                $message='';
                $file_count=count($file);
                for ($i=0; $i<$file_count; $i++) {
                    $image = new Image();
                    if (($file[$i] instanceof UploadedFile) && ($file[$i]->getError() == 0)) {

                        if (($file[$i]->getSize() < 2000000000)) {
                            $originalName = $file[$i]->getClientOriginalName();
                            $fileInfos = pathinfo($originalName);
                            $file_type = $fileInfos['extension'];
                            if (in_array(strtolower($file_type), $this->valideTypes)) {
                                //Start Uploading File

                                $document = new Document();
                                $document->setFile($file[$i]);
                                $document->setUploadDirectory($this->getParameter('kernel.root_dir') . '/../web/uploads');
                                $document->setSubDirectory('uploads');
                                $document->processFile();
                                $uploadedURL = '/uploads/' . $document->getSubDirectory() .
                                    '/' . $file[$i]->getBasename();
                                $fileFullPath = $this->getParameter('kernel.root_dir') . '/../web/uploads/uploads/'.$file[$i]->getBasename();
                                if($file_type =='dcm') {
                                    $d = new \dicom_convert;
                                    $d->file = $fileFullPath;
                                    $d->dcm_to_jpg();
                                    // delete the tmp file
                                    \unlink($fileFullPath);
                                    $uploadedURL.='.jpg';
                                }
                                $demande->setUrl($uploadedURL);
                                $image->setUrl($uploadedURL);
                                $image->setDemande($demande);
                                $demande->addDemandeImage($image);
                            } else {
                                $status = 'failed';
                                $message = 'Invalid File Type';
                            }
                        } else {
                            $status = 'failed';
                            $message = 'Size exceeds limit';
                        }
                    } else {
                        return $this->redirect($this->generateUrl("requester_profile", array('popup' => $popCreation)));
                    }

                }

                // get selected categories
                $categories = $request->request->get("categories");
                foreach ($categories as $catId) {
                    $categorie = $em->getRepository('IdvBundle:Categorie')->find((int)$catId);
                    $demande->addCategory($categorie);
                }
                $em->persist($demande);
                $em->flush();
            }
        }

        return $this->redirect($this->generateUrl("requester_profile", array('popup' => $popCreation)));
    }

    /**
     * @Route("/requester/request/biologiste/create", name="request_biologiste_create")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function createRequestBiologisteAction(Request $request)
    {

        $popCreation ='hide';
        include __DIR__.'/../External/class_dicom.php';
        if ($request->getSession()->get("requesterSession")){
            if ($request->isMethod("post")) {
                $popCreation = 'show';
                $em = $this->getDoctrine()->getManager();
                $rep_owner = $em->getRepository("IdvBundle:Utilisateur");

                $owner = $rep_owner->find($request->getSession()->get("requesterSession")->getId());

                $demande = new Demande();
                $demande->setTitre($request->request->get("titre"));
                $demande->setDescription($request->request->get("description"));
                $demande->setDatePublication(new \DateTime('now'));
                $date = $request->request->get("aaaa")."-".$request->request->get("mm")."-".$request->request->get("jj");
                $demande->setEcheance(new \DateTime($date));
                $demande->setVerouiller(1);
                $demande->settype(demande::TYPE_BIOLOGISTE);
                $demande->setOwner($owner);
                $demande->setCode($this->getRandomString(5));


                $file = $request->files->get("img");
                $status = 'success';
                $uploadedURL='';
                $message='';
                $file_count=count($file);
                for ($i=0; $i<$file_count; $i++) {
                    $image = new Image();
                    if (($file[$i] instanceof UploadedFile) && ($file[$i]->getError() == 0)) {

                        if (($file[$i]->getSize() < 2000000000)) {
                            $originalName = $file[$i]->getClientOriginalName();
                            $fileInfos = pathinfo($originalName);
                            $file_type = $fileInfos['extension'];
                            if (in_array(strtolower($file_type), $this->valideTypes)) {
                                //Start Uploading File
                                $document = new Document();
                                $document->setFile($file[$i]);
                                $document->setUploadDirectory($this->getParameter('kernel.root_dir') . '/../web/uploads');
                                $document->setSubDirectory('uploads');
                                $document->processFile();
                                $uploadedURL = '/uploads/' . $document->getSubDirectory() .
                                    '/' . $file[$i]->getBasename();
                                $fileFullPath = $this->getParameter('kernel.root_dir') . '/../web/uploads/uploads/'.$file[$i]->getBasename();
                                if($file_type =='dcm') {
                                    $d = new \dicom_convert;
                                    $d->file = $fileFullPath;
                                    $d->dcm_to_jpg();
                                    // delete the tmp file
                                    \unlink($fileFullPath);
                                    $uploadedURL.='.jpg';
                                }
                                $demande->setUrl($uploadedURL);
                                $image->setUrl($uploadedURL);
                                $image->setDemande($demande);
                                $demande->addDemandeImage($image);
                            } else {
                                $status = 'failed';
                                $message = 'Invalid File Type';
                            }
                        } else {
                            $status = 'failed';
                            $message = 'Size exceeds limit';
                        }
                    } else {
                        return $this->redirect($this->generateUrl("requester_profile", array('popup' => $popCreation)));
                    }

                }

                $em->persist($demande);
                $em->flush();
            }
        }

        return $this->redirect($this->generateUrl("requester_profile", array('popup' => $popCreation)));
    }

    /**
     * @Route("/ajax/remove/demande/{demande}", name="ajax_remove_demande",  options={"expose"=true})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function removeDemandeAction(Demande $demande)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($demande);
        $em->flush();

        return new JsonResponse(['success' => true]);
    }

    /**
     * @Route("/ajax/add/category/{name}", name="ajax_add_category", options={"expose"=true})
     * @param $name
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function addCategoryAction($name)
    {
        $em = $this->getDoctrine()->getManager();
        $category = new Categorie();
        $category->setNom($name);
        $em->persist($category);
        $em->flush();

        return new JsonResponse(['success' => true]);
    }


    /**
     * @Route("/valider/analyse/{image}", name="validate_image_analyse")
     * @param Image $image
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function validateAnalyseSegmentationAction(Image $image)
    {
        $image->setValidated(true);
        /** @var AnnotateHelper $annotationHelper */
        $annotationHelper = $this->get('annotation_helper');

        if($image->getDemande()->getType()== Demande::TYPE_SEGMENTATION){
            $result = $annotationHelper->calculateAvgSegmentationImage($image);
            if(!empty($result)){
                $result = reset($result);
                $image->setResult($result);
            }
        }

        $result = [];
        if($image->getDemande()->getType()== Demande::TYPE_ANNOTATION){
            $categories  = $image->getDemande()->getCategories();
            /** @var Categorie $category */
            foreach ($categories as $category) {
                $result[$category->getNom()] = $annotationHelper->calculateReponse($image, $category);
            }
            if(!empty($result)){
                $image->setResult($result);
            }
        }

        if($image->getDemande()->getType()== Demande::TYPE_BIOLOGISTE){
            $result['quality'] = $annotationHelper->calculateReponseBiologiste($image);
            $image->setResult($result);
        }

        $annotationHelper->persist($image);
        $demande = $image->getDemande();
        if($demande->getType() == Demande::TYPE_ANNOTATION)
            return $this->redirect($this->generateUrl("requester_show_image_annotation",  ["image" => $image->getId()]));
        if($demande->getType() == Demande::TYPE_SEGMENTATION)
            return $this->redirect($this->generateUrl("requester_show_image",  ["image" => $image->getId(), "group" => $image->getGroup()->getId()]));

        if($demande->getType() == Demande::TYPE_BIOLOGISTE)
            return $this->redirect($this->generateUrl("requester_show_image_biologiste",  ["image" => $image->getId()]));
    }

    /**
     * @Route("/reject/analyse/{image}", name="reject_image_analyse")
     * @param Image $image
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function rejectImageAnalyseAction(Image $image)
    {
        /** @var AnnotateHelper $annotationHelper */
        $annotationHelper = $this->get('annotation_helper');

        if($image->getDemande()->getType()== Demande::TYPE_SEGMENTATION){
            $annotations = $image->getAnnotations();
            foreach ($annotations as $annotation) {
                $annotationHelper->remove($annotation);
            }
        }
        if($image->getDemande()->getType()== Demande::TYPE_ANNOTATION || $image->getDemande()->getType()== Demande::TYPE_BIOLOGISTE){
            $reponses = $image->getReponses();
            foreach ($reponses as $reponse) {
                $annotationHelper->remove($reponse);
            }
        }

        $demande = $image->getDemande();
        if($demande->getType() == Demande::TYPE_ANNOTATION)
            return $this->redirect($this->generateUrl("requester_show_image_annotation",  ["image" => $image->getId()]));
        if($demande->getType() == Demande::TYPE_SEGMENTATION)
            return $this->redirect($this->generateUrl("requester_show_image",  ["image" => $image->getId(), "group" => $image->getGroup()->getId()]));

        if($demande->getType() == Demande::TYPE_BIOLOGISTE)
            return $this->redirect($this->generateUrl("requester_show_image_biologiste",  ["image" => $image->getId()]));
    }

    public function sendForgetPassword($email, $password)
    {
        $message = new \Swift_Message('Mot de passe oublié!');
        $message
            ->setFrom('idvparisdescartes@gmail.com')
            ->setTo($email)
            ->setBody(
                $this->renderView(
                    '@Idv/Mail/forgot_password.html.twig',
                    array('password' => $password)
                ),
                'text/html'
            )
        ;
        $this->get('mailer')->send($message);
    }


}
