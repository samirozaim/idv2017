<?php
/**
 * Created by PhpStorm.
 * User: Abid
 * Date: 27/02/2017
 * Time: 23:44
 */

namespace IdvBundle\Controller;

use IdvBundle\Entity\Categorie;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends Controller
{

    /**
     *
     * @Route("/admin/login", name="login_admin")
     */
    public function loginAdminAction(Request $request){

        if($request->isMethod("post")){

            $em = $this->getDoctrine()->getManager();
            $rep = $em->getRepository("IdvBundle:Admin");
            $admin = $rep->findOneBy(array(
                "email"=> $request->request->get("email"),
                "password"=> $request->request->get("password")
            ));

            if($admin){

                $request->getSession()->set("adminSession", $admin/*,array(
                    "id"=>$admin->getId(),
                    "nom"=>$admin->getNom(),
                    "prenom"=>$admin->getPrenom(),
                    "email"=> $admin->getEmail()
                )*/);

                return $this->redirect($this->generateUrl("show_admin"));
            }else{

                //session erreur

            }
        }
        return $this->render("IdvBundle:Admin:admin_login.html.twig");

    }

    /**
     *
     * @Route("/admin/show", name="show_admin")
     */
    public function showAdminAction(Request $request){

        $admin = $request->getSession()->get("adminSession");
        return $this->render("IdvBundle:Admin:profil.html.twig", array("admin"=> $admin));
    }

    /**
     *
     * @Route("/admin/edit", name="edit_admin")
     */
    public function editAdminAction(Request $req){

        if($req->isMethod('post')){
            $em = $this->getDoctrine()->getManager();
            $rep = $em->getRepository("IdvBundle:Admin");
            $admin = $rep->find($req->getSession()->get("adminSession")->getId());

            if($admin){
                $nom = $req->request->get("nom");
                $prenom = $req->request->get("prenom");
                $email = $req->request->get("email");
                $op = $req->request->get("o_password");
                $np = $req->request->get("n_password");
                $cp = $req->request->get("c_password");

                if($admin->getPassword() == $op){
                    if(!empty($nom) && !empty($prenom) && !empty($email)){
                        $admin->setNom($nom);
                        $admin->setPrenom($prenom);
                        $admin->setEmail($email);
                        if(!empty($np) && $np == $cp)
                            $admin->setPassword($np);

                        $em->flush();
                        $req->getSession()->set("adminSession", $admin);
                    }
                }
            }
        }
        return $this->redirect($this->generateUrl("show_admin"));
    }

    /**
     *
     * @Route("/admin/category/create", name="create_category")
     */
    public function createCategoryAction(Request $request){

        if($request->isMethod("post")){

            $em = $this->getDoctrine()->getManager();
            $rep = $em->getRepository("IdvBundle:Categorie");
            $result = $rep->findAll();
            $cat = new Categorie();
            $nom = $request->request->get("nom");
            $des = $request->request->get("description");

            if(!empty($des) && !empty($nom)){

                $cat->setNom($request->request->get("nom"));
                $cat->setDescription($request->request->get("description"));

                $em->persist($cat);
                $em->flush();
                //session msg
            }
            return $this->redirect($this->generateUrl("show_admin", array("result"=>$result)));
        }
    }

}