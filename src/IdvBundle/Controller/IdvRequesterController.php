<?php


namespace IdvBundle\Controller;


use IdvBundle\Entity\Annotation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use IdvBundle\Models\Document;

class IdvRequesterController extends Controller
{

    /**
     *
     * @Route("/crowder/login", name="crowder_login")
     */
    public function loginRequester(Request $req){


        if($req->getSession()->isStarted() && $req->getSession()->get("crowderSession")){

            return $this->redirect($this->generateUrl("crowder_profile"));

        }else{

            if($req->isMethod("post")){

                $em = $this->getDoctrine()->getManager();
                $rep = $em->getRepository("IdvBundle:Requester");
                $email = $req->request->get("email");
                $passwd = $req->request->get("password");
                $crowder = $rep->findOneBy(array("email"=>$email, "password"=>$passwd));

                if($crowder){

                    if(!$req->getSession()->isStarted())
                        $req->getSession()->start();

                    $req->getSession()->set("crowderSession", $crowder);

                    return $this->redirect($this->generateUrl("crowder_profile"));

                }else{

                    return $this->redirect($this->generateUrl("sign_in"));
                }

            }
            return $this->redirect($this->generateUrl("sign_in"));
        }

    }

    /**
     *
     * @Route("/crowder/profile", name="crowder_profile")
     */
    public function showCrowderAction(Request $req){

        if($req->getSession()->get("crowderSession")){
        $em = $this->getDoctrine()->getManager();
        $rep = $em->getRepository("IdvBundle:Requester");

        $crowder = $rep->find($req->getSession()->get("crowderSession")->getId());
        return $this->render("IdvBundle:Requester:profil.html.twig", array("user"=> $crowder));
        }
        $url = $this->generateUrl("sign_in");
        return $this->redirect($url);
    }

    /**
     *
     * @Route("/crowder/profile/edit", name="crowder_profile_edit")
     */
    public function editCrowderAction(Request $req){

        if($req->isMethod('post')){
            $em = $this->getDoctrine()->getManager();
            $rep = $em->getRepository("IdvBundle:Requester");
            $crowder = $rep->find($req->getSession()->get("crowderSession")->getId());
            if($crowder){
                $nom = $req->request->get("nom");
                $prenom = $req->request->get("prenom");
                $email = $req->request->get("email");
                $username = $req->request->get("username");
                $description = $req->request->get("description");
                $op = $req->request->get("o_password");
                $np = $req->request->get("n_password");
                $cp = $req->request->get("c_password");
                if($req->files->get("profile")){
                    $file = $req->files->get("profile");
                    $status = 'success';
                    $uploadedURL='';
                    $message='';
                    if(($file instanceof UploadedFile) && ($file->getError() == 0)){
                        if (($file->getSize() < 2000000000)) {
                            $originalName = $file->getClientOriginalName();
                            $name_array = explode('.', $originalName);
                            $file_type = $name_array[sizeof($name_array) - 1];
                            $valid_filetypes = array('jpg', 'jpeg', 'bmp', 'png');
                            if (in_array(strtolower($file_type), $valid_filetypes)) {
                                //Start Uploading File

                                $document = new Document();
                                $document->setFile($file);
			        $document->setUploadDirectory($this->getParameter('kernel.root_dir').'/../web/uploads');
                                $document->setSubDirectory('profiles');
                                $document->processFile();
                                $uploadedURL = '/uploads/'. $document->getSubDirectory() .
                                    '/' . $file->getBasename();

                                $crowder->setUrl($uploadedURL);
                            } else {
                                $status = 'failed';
                                $message = 'Invalid File Type';
                            }
                        } else {
                            $status = 'failed';
                            $message = 'Size exceeds limit';
                        }
                    }
                }

                if($crowder->getPassword() == $op){
                    if(!empty($nom) && !empty($prenom) && !empty($email) && !empty($username)){
                        $crowder->setNom($nom);
                        $crowder->setPrenom($prenom);
                        $crowder->setEmail($email);
                        $crowder->setUsername($username);
                        $crowder->setDescription($description);
                        if(!empty($np) && $np == $cp)
                            $crowder->setPassword($np);
			

            		$em = $this->getDoctrine()->getManager();
            		$em->persist($crowder);
                        $em->flush();

                        $req->getSession()->set("crowder", $crowder);
                    }
                }
            }
        }
        return $this->redirect($this->generateUrl("crowder_profile"));
    }

    /**
     *
     * @Route("/crowder/annotation/show/{ida}", name="crowder_annotation_show_id", options={"expose"=true})
     */
    public function showCrowderAnnotationAction($ida){

        $em = $this->getDoctrine()->getManager();
        $rep = $em->getRepository("IdvBundle:Annotation");
        /** @var Annotation $annotation */
        $annotation = $rep->findOneBy(array("id"=>$ida));
        $resp = array(
            "xPoint"=>$annotation->getXPoint(),
            "yPoint"=>$annotation->getYPoint(),
            "height"=>$annotation->getHeight(),
            "width"=>$annotation->getWidth(),
            "description"=> $annotation->getDescription(),
            "url"=>$annotation->getImageAnnotation()->getUrl(),
            "titre"=> $annotation->getImageAnnotation()->getDemande()->getTitre(),
            "confiance"=>$annotation->getConfiance()
        );


        return new Response(json_encode($resp));
    }

    /**
     *
     * @Route("/crowder/logout", name="logout_crowder")
     */
    public function deconnAction(Request $req)
    {

        $req->getSession()->remove("requesterSession");
        $req->getSession()->remove("crowderSession");
        $req->getSession()->clear();
        return $this->redirect($this->generateUrl('home'));
    }


}
