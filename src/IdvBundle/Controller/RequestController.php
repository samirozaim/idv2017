<?php
/**
 * Created by PhpStorm.
 * User: Abid
 * Date: 26/02/2017
 * Time: 18:52
 */

namespace IdvBundle\Controller;

use IdvBundle\Entity\Annotation;
use IdvBundle\Entity\Categorie;
use IdvBundle\Entity\Demande;
use IdvBundle\Entity\Group;
use IdvBundle\Entity\Image;
use IdvBundle\Helper\AnnotateHelper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\DateTime;


class RequestController extends Controller
{

    /**
     *
     * @Route("/requests/show", name="show_requests")
     */
    public function showRequestsAction(Request $request){

        if ($request->getSession()->get("requesterSession") || $request->getSession()->get("crowderSession")) {

            $em = $this->getDoctrine()->getManager();
            $d_now = new \DateTime('now');
            $f = $d_now->format('Y-m-d');
            $q = $em->createQuery("
                SELECT d
                FROM IdvBundle:Demande d
                WHERE d.echeance > :echeance AND d.verouiller = 1
            ")->setParameter('echeance', $f);

            $r = $q->getResult();

            return $this->render("IdvBundle:Request:show.html.twig", array("result" => $r));
        }

        return $this->redirect($this->generateUrl("sign_in"));
    }

    /**
     *
     * @Route("/requests/show/{id}", name="show_requests_id")
     */
    public function showRequestAction($id, Request $request){
        if ($request->getSession()->get("requesterSession") || $request->getSession()->get("crowderSession")) {
            $rep = $this->getDoctrine()->getManager()->getRepository("IdvBundle:Demande");
            $result = $rep->find($id);

            return $this->render("IdvBundle:Request:request.html.twig", array("result"=> $result));
        }

        return $this->redirect($this->generateUrl("sign_in"));
    }

    /**
     * @Route("/requests/segmentation", name="segmentation_request")
     */
    public function segmentationRequestAction(Request $request)
    {
        /** @var AnnotateHelper $annotateHelper */
        $annotateHelper = $this->get('annotation_helper');
        if ($request->getSession()->get("requesterSession") || $request->getSession()->get("crowderSession")) {
            if($request->isMethod("post")){
                /** @var Image $image */
                $image = $annotateHelper->getRepository('IdvBundle:Image')->find($request->request->get("request_id"));
                $annotateHelper->resetOldAnnotationByImage($image);
                $crowder = $annotateHelper->getCurrentCrowder();
                $annotation = new Annotation();
                $annotation->setXPoint($request->request->get("xpoint"));
                $annotation->setYPoint($request->request->get("ypoint"));
                $annotation->setDescription($request->request->get("description"));
                $annotation->setWidth($request->request->get("width"));
                $annotation->setHeight($request->request->get("height"));
                $annotation->setConfiance($request->request->get("confiance"));
                $annotation->setImageAnnotation($image);
                $annotation->setRequester($crowder);
                $annotateHelper->persist($annotation);
                $nextImage = $annotateHelper->getNextImageGroup($image);
                //dump($nextImage);die;
                //if(!$nextImage) return $this->redirect($this->generateUrl("crowder_profile"));
                return $this->redirect($this->generateUrl('annotate_image', ['image' => $image->getId(), 'group' => $image->getGroup()->getId()]));

            }
            return $this->redirect($this->generateUrl("show_requests"));
        }

        return $this->redirect($this->generateUrl("sign_in"));
    }

    /**
     *
     * @Route("/request/analyze/{id}", name="request_analyse")
     */
    public function analyzeAction(Request $req, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $rep = $em->getRepository("IdvBundle:Annotation");
        $ann = $rep->findBy(array("demandeAnnotation"=>$id), array("requester"=>"desc"));

        $params = [];
        $userId = 0;
        $prefix = "";
        $suffix = "";
        foreach($ann as $an){
            if($an->getRequester()->getID() != $userId){
                $params[] = $prefix.$suffix;
                $suffix = "";
                $userId = $an->getRequester()->getID();
                $prefix = $userId.";".$an->getRequester()->getReputation().";";
            }
            $suffix .= $an->getDescription().";".$an->getConfiance().";";
        }
        $params[] = $prefix.$suffix;
        $params = substr(implode("|", $params), 1);
        $params = str_replace(" ","-",$params);
        //echo $params;
        $result = self::analyze($params);
        return $this->render("IdvBundle:Request:result.html.twig", array("result"=> $result));
    }

    public function analyze($operation){
        $url = "http://pf-01.lab.parisdescartes.fr:1241/apriori/analyseur?operation=".$operation;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
        curl_setopt($ch, CURLOPT_HEADER, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $content = curl_exec($ch);
        curl_close($ch);
        preg_match_all('/^Location:(.*)$/mi', $content, $matches);
        if(empty($matches[1]))
            $content = "Server error1";
        else{
            $content = trim($matches[1][0]);
            $needle = 'resultat=';
            $pos = strpos($content, $needle);
            if(!$pos)
                $content = "Server error2";
            else
                $content = substr($content, $pos + strlen($needle));
        }
        return $content;
    }


    /**
     *
     * @Route("/requests/check_show", name="check_request")
     */
    public function checkRequestAction(Request $request){
        if ($request->getSession()->get("requesterSession") || $request->getSession()->get("crowderSession")) {
            if ($request->request->get("ID"))
            {
                $id = $request->request->get("ID");
                $code = $request->request->get("CODE");
                $rep = $this->getDoctrine()->getManager()->getRepository("IdvBundle:Demande");
                /** @var Demande $result */
                $result = $rep->find($id);
                $originalCode = $result->getCode();
                if($code == $originalCode){
                    /** @var Image $image */
                    $image = $result->getDemandeImages()->first();
                    if($result->getType() == Demande::TYPE_ANNOTATION)
                        return $this->redirect($this->generateUrl("annotate_image_annotation",  ["image" => $image->getId()]));

                    if($result->getType() == Demande::TYPE_SEGMENTATION)
                        return $this->redirect($this->generateUrl("annotate_image",  ["image" => $image->getId(), 'group' => $image->getGroup()->getId()]));

                    if($result->getType() == Demande::TYPE_BIOLOGISTE)
                        return $this->redirect($this->generateUrl("annotate_image_biologiste",  ["image" => $image->getId()]));
                } else {
                    return $this->redirect($this->generateUrl("show_requests"));
                }
            }

        }

        return $this->redirect($this->generateUrl("sign_in"));
    }

    /**
     * @Route("/request/group/{group}/image/{image}/annotate", name="annotate_image")
     * @param Request $request
     * @param Image $image
     * @return Response
     */
    public function annotateImageAction(Request $request, Image $image, Group $group)
    {
        /** @var AnnotateHelper $annotateHelper */
        $annotateHelper = $this->get('annotation_helper');

        $annotation = $annotateHelper->getRepository('IdvBundle:Annotation')->findOneBy(['requester' => $annotateHelper->getCurrentCrowder(), 'imageAnnotation' => $image]);
        $reponses = $annotateHelper->getRepository('IdvBundle:Reponse')->findBy(['image' => $image, 'requester' => $annotateHelper->getCurrentCrowder()]);
        return $this->render("IdvBundle:Request:request.html.twig", array("currentImage"=> $image, 'userAnnotation' => $annotation, 'reponses' => $reponses, 'group' => $group));
    }

    /**
     * @Route("/request/group/annotation/{image}/annotate", name="annotate_image_annotation")
     * @param Request $request
     * @param Image $image
     * @return Response
     */
    public function annotateImageAnnotationAction(Request $request, Image $image)
    {
        /** @var AnnotateHelper $annotateHelper */
        $annotateHelper = $this->get('annotation_helper');

        $annotation = $annotateHelper->getRepository('IdvBundle:Annotation')->findOneBy(['requester' => $annotateHelper->getCurrentCrowder(), 'imageAnnotation' => $image]);
        $reponses = $annotateHelper->getRepository('IdvBundle:Reponse')->findBy(['image' => $image, 'requester' => $annotateHelper->getCurrentCrowder()]);
        return $this->render("IdvBundle:Request:request_annotation.html.twig", array("currentImage"=> $image, 'userAnnotation' => $annotation, 'reponses' => $reponses));
    }

    /**
     * @Route("/request/group/biologiste/{image}/annotate", name="annotate_image_biologiste")
     * @param Request $request
     * @param Image $image
     * @return Response
     */
    public function annotateImageBiologisteAction(Request $request, Image $image)
    {
        /** @var AnnotateHelper $annotateHelper */
        $annotateHelper = $this->get('annotation_helper');

        $annotation = $annotateHelper->getRepository('IdvBundle:Annotation')->findOneBy(['requester' => $annotateHelper->getCurrentCrowder(), 'imageAnnotation' => $image]);
        $reponse = $annotateHelper->getRepository('IdvBundle:Reponse')->findOneBy(['image' => $image, 'requester' => $annotateHelper->getCurrentCrowder()]);
        return $this->render("IdvBundle:Request:request_biologiste.html.twig", array("currentImage"=> $image, 'userAnnotation' => $annotation, 'response' => $reponse));
    }



    /**
     * @Route("/requests/annotate", name="annotate_request")
     */
    public function annotateRequestAction(Request $request)
    {
        /** @var AnnotateHelper $annotateHelper */
        $annotateHelper = $this->get('annotation_helper');
        if ($request->getSession()->get("requesterSession") || $request->getSession()->get("crowderSession")) {
            if($request->isMethod("post")){
                /** @var Image $image */
                $image = $annotateHelper->getRepository('IdvBundle:Image')->find($request->request->get("request_id"));
                /** @var Demande $demande */
                $availableCategories = $image->getDemande()->getCategories();
                /** @var Categorie $availableCategory */
                foreach ($availableCategories as $availableCategory) {
                    $sReponse = $request->request->get("cat_".$availableCategory->getId());
                    $annotateHelper->createReponseFromStringAndImage($image, $availableCategory, $sReponse);
                }

                return $this->redirect($this->generateUrl('annotate_image_annotation', ['image' => $image->getId()]));

            }
            return $this->redirect($this->generateUrl("show_requests"));
        }

        return $this->redirect($this->generateUrl("sign_in"));
    }
    /**
     * @Route("/requests/annotate/biologiste", name="annotate_biologiste_request")
     */
    public function annotateBiologisteRequestAction(Request $request)
    {
        /** @var AnnotateHelper $annotateHelper */
        $annotateHelper = $this->get('annotation_helper');
        if ($request->getSession()->get("requesterSession") || $request->getSession()->get("crowderSession")) {
            if($request->isMethod("post")){
                /** @var Image $image */
                $image = $annotateHelper->getRepository('IdvBundle:Image')->find($request->request->get("request_id"));
                $sReponse = $request->request->get("quality_choice");
                $annotateHelper->createChoiceReponseFromStringAndImage($image,$sReponse);

                return $this->redirect($this->generateUrl('annotate_image_biologiste', ['image' => $image->getId()]));

            }
            return $this->redirect($this->generateUrl("show_requests"));
        }

        return $this->redirect($this->generateUrl("sign_in"));
    }

}