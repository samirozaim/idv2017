<?php

namespace IdvBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/other")
     */
    public function otherAction()
    {
        return $this->render('IdvBundle:Default:index.html.twig');
    }
}
