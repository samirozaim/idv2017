<?php

namespace IdvBundle\Models;

use Symfony\Component\DependencyInjection\Container;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\User;

class HelperModel
{
    protected $container;

    /**
     * AppointmentHelper constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @param $repo
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    public function getRepository($repo)
    {
        return $this->getContainer()->get('doctrine')->getRepository($repo);
    }

    /**
     * @return EntityManager
     */
    public function _getEM()
    {
        return $this->getContainer()->get('doctrine')->getManager();
    }

    /**
     * @return Container
     */
    public function getContainer()
    {
        return $this->container;
    }


    /**
     * @param $object
     */
    public function persist($object)
    {
        $em = $this->_getEM();
        $em->persist($object);
        $em->flush();
    }

    /**
     * @param $object
     */
    public function remove($object)
    {
        $em = $this->_getEM();
        $em->remove($object);
        $em->flush();
    }

    /**
     * Return service
     * @param $name
     * @return object
     */
    public function get($name)
    {
        return $this->container->get($name);
    }

    public function getCurrentCrowder()
    {
        $id = $this->get('session')->get("crowderSession")->getId();
        return $this->getRepository("IdvBundle:Requester")->find($id);
    }
}