<?php

namespace IdvBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Annotation
 *
 * @ORM\Table(name="annotation")
 * @ORM\Entity(repositoryClass="IdvBundle\Repository\AnnotationRepository")
 */
class Annotation
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="xPoint", type="float")
     */
    private $xPoint;

    /**
     * @var float
     *
     * @ORM\Column(name="yPoint", type="float")
     */
    private $yPoint;

    /**
     * @var float
     *
     * @ORM\Column(name="height", type="float")
     */
    private $height;

    /**
     * @var float
     *
     * @ORM\Column(name="width", type="float")
     */
    private $width;

    /**
     * @var
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="confiance", type="integer")
     */
    private $confiance;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Requester", inversedBy="r_annotations")
     * @ORM\JoinColumn(name="requester_id", referencedColumnName="id")
     */
    private $requester;

    /**
     *
     * @ORM\ManyToOne(targetEntity="IdvBundle\Entity\Image", inversedBy="annotations")
     * @ORM\JoinColumn(name="image_id", referencedColumnName="id")
     */
    private $imageAnnotation;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set xPoint
     *
     * @param float $xPoint
     * @return Annotation
     */
    public function setXPoint($xPoint)
    {
        $this->xPoint = $xPoint;

        return $this;
    }

    /**
     * Get xPoint
     *
     * @return float 
     */
    public function getXPoint()
    {
        return $this->xPoint;
    }

    /**
     * Set yPoint
     *
     * @param float $yPoint
     * @return Annotation
     */
    public function setYPoint($yPoint)
    {
        $this->yPoint = $yPoint;

        return $this;
    }

    /**
     * Get yPoint
     *
     * @return float 
     */
    public function getYPoint()
    {
        return $this->yPoint;
    }

    /**
     * Set requester
     *
     * @param \IdvBundle\Entity\Requester $requester
     * @return Annotation
     */
    public function setRequester(\IdvBundle\Entity\Requester $requester = null)
    {
        $this->requester = $requester;

        return $this;
    }

    /**
     * Get requester
     *
     * @return \IdvBundle\Entity\Requester 
     */
    public function getRequester()
    {
        return $this->requester;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Annotation
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set height
     *
     * @param float $height
     * @return Annotation
     */
    public function setHeight($height)
    {
        $this->height = $height;
    
        return $this;
    }

    /**
     * Get height
     *
     * @return float 
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set width
     *
     * @param float $width
     * @return Annotation
     */
    public function setWidth($width)
    {
        $this->width = $width;
    
        return $this;
    }

    /**
     * Get width
     *
     * @return float 
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set confiance
     *
     * @param integer $confiance
     * @return Annotation
     */
    public function setConfiance($confiance)
    {
        $this->confiance = $confiance;
    
        return $this;
    }

    /**
     * Get confiance
     *
     * @return integer 
     */
    public function getConfiance()
    {
        return $this->confiance;
    }

    /**
     * Set imageAnnotation
     *
     * @param \IdvBundle\Entity\Image $imageAnnotation
     * @return Annotation
     */
    public function setImageAnnotation(\IdvBundle\Entity\Image $imageAnnotation = null)
    {
        $this->imageAnnotation = $imageAnnotation;

        return $this;
    }

    /**
     * Get imageAnnotation
     *
     * @return \IdvBundle\Entity\Image 
     */
    public function getImageAnnotation()
    {
        return $this->imageAnnotation;
    }

    /**
     *
     * Constructor
     */
    public function __construct()
    {
    }

}
