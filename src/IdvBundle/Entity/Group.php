<?php
/**
 * Created by PhpStorm.
 * User: samir
 * Date: 12/05/2017
 * Time: 15:55
 */

namespace IdvBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Image
 *
 * @ORM\Table(name="group_image")
 * @ORM\Entity(repositoryClass="IdvBundle\Repository\GroupRepository")
 */

class Group{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Image", mappedBy="group", cascade={"persist", "remove"})
     */
    private $images;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add images
     *
     * @param \IdvBundle\Entity\Image $images
     * @return Group
     */
    public function addImage(\IdvBundle\Entity\Image $images)
    {
        $this->images[] = $images;

        return $this;
    }

    /**
     * Remove images
     *
     * @param \IdvBundle\Entity\Image $images
     */
    public function removeImage(\IdvBundle\Entity\Image $images)
    {
        $this->images->removeElement($images);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImages()
    {
        return $this->images;
    }
}
