<?php
/**
 * Created by PhpStorm.
 * User: samir
 * Date: 12/05/2017
 * Time: 15:55
 */

namespace IdvBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Image
 *
 * @ORM\Table(name="image")
 * @ORM\Entity(repositoryClass="IdvBundle\Repository\ImageRepository")
 */

class Image{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @ORM\ManyToOne(targetEntity="Demande", inversedBy="demande_images")
     * @ORM\JoinColumn(name="demande_id", referencedColumnName="id")
     */
    private $demande;

    /**
     * @ORM\ManyToOne(targetEntity="Group", inversedBy="images")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     */
    private $group;

    /**
     * @ORM\OneToMany(targetEntity="Annotation", mappedBy="imageAnnotation", cascade={"remove"})
     */
    private $annotations;

    /**
     * @ORM\OneToMany(targetEntity="IdvBundle\Entity\Reponse", mappedBy="image", cascade={"remove"})
     */
    private $reponses;

    /**
     * @var bool
     *
     * @ORM\Column(name="validated", type="boolean")
     */
    private $validated =false;

    /**
     * @var bool
     *
     * @ORM\Column(name="result", type="json_array", nullable=true)
     */
    private $result;

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getDemande()
    {
        return $this->demande;
    }

    /**
     * @param mixed $demande
     */
    public function setDemande($demande)
    {
        $this->demande = $demande;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->annotations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add annotations
     *
     * @param \IdvBundle\Entity\Annotation $annotations
     * @return Image
     */
    public function addAnnotation(\IdvBundle\Entity\Annotation $annotations)
    {
        $this->annotations[] = $annotations;

        return $this;
    }

    /**
     * Remove annotations
     *
     * @param \IdvBundle\Entity\Annotation $annotations
     */
    public function removeAnnotation(\IdvBundle\Entity\Annotation $annotations)
    {
        $this->annotations->removeElement($annotations);
    }

    /**
     * Get annotations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAnnotations()
    {
        return $this->annotations;
    }

    /**
     * Add reponses
     *
     * @param \IdvBundle\Entity\Reponse $reponses
     * @return Image
     */
    public function addReponse(\IdvBundle\Entity\Reponse $reponses)
    {
        $this->reponses[] = $reponses;

        return $this;
    }

    /**
     * Remove reponses
     *
     * @param \IdvBundle\Entity\Reponse $reponses
     */
    public function removeReponse(\IdvBundle\Entity\Reponse $reponses)
    {
        $this->reponses->removeElement($reponses);
    }

    /**
     * Get reponses
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReponses()
    {
        return $this->reponses;
    }

    /**
     * Set group
     *
     * @param \IdvBundle\Entity\Group $group
     * @return Image
     */
    public function setGroup(\IdvBundle\Entity\Group $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \IdvBundle\Entity\Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set validated
     *
     * @param boolean $validated
     * @return Image
     */
    public function setValidated($validated)
    {
        $this->validated = $validated;

        return $this;
    }

    /**
     * Get validated
     *
     * @return boolean 
     */
    public function getValidated()
    {
        return $this->validated;
    }

    /**
     * Set result
     *
     * @param array $result
     * @return Image
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result
     *
     * @return array 
     */
    public function getResult()
    {
        return $this->result;
    }
}
