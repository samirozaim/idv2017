<?php

namespace IdvBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Demande
 *
 * @ORM\Table(name="demande")
 * @ORM\Entity(repositoryClass="IdvBundle\Repository\DemandeRepository")
 */
class Demande
{
    const TYPE_SEGMENTATION = "segmentation";
    const TYPE_ANNOTATION  = "annotation";
    const TYPE_BIOLOGISTE = 'biologiste';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;


    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;


    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255)
     */
    private $code;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datePublication", type="date")
     */
    private $datePublication;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="echeance", type="date")
     */
    private $echeance;

    /**
     * @var integer
     *
     * @ORM\Column(name="verouiller", type="integer")
     */
    private $verouiller;

    /**
     * @var
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur", inversedBy="user_demandes")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;

    /**
     *
     * @ORM\OneToMany(targetEntity="Image", mappedBy="demande", cascade={"persist", "remove"})
     */
    private $demandeImages;
    
    /**
     * @ORM\ManyToMany(targetEntity="IdvBundle\Entity\Categorie")
     * @ORM\JoinTable(name="demande_categorie",
     *      joinColumns={@ORM\JoinColumn(name="demande_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="categorie_id", referencedColumnName="id")}
     *      )
     */
    private $categories;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Demande
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Demande
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set datePublication
     *
     * @param \DateTime $datePublication
     * @return Demande
     */
    public function setDatePublication($datePublication)
    {
        $this->datePublication = $datePublication;

        return $this;
    }

    /**
     * Get datePublication
     *
     * @return \DateTime 
     */
    public function getDatePublication()
    {
        return $this->datePublication;
    }

    /**
     * Set echeance
     *
     * @param \DateTime $echeance
     * @return Demande
     */
    public function setEcheance($echeance)
    {
        $this->echeance = $echeance;

        return $this;
    }

    /**
     * Get echeance
     *
     * @return \DateTime 
     */
    public function getEcheance()
    {
        return $this->echeance;
    }

    /**
     * Set categorie
     *
     * @param \IdvBundle\Entity\Categorie $categorie
     * @return Demande
     */
    public function setCategorie(\IdvBundle\Entity\Categorie $categorie = null)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return \IdvBundle\Entity\Categorie 
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set owner
     *
     * @param \IdvBundle\Entity\Utilisateur $owner
     * @return Demande
     */
    public function setOwner(\IdvBundle\Entity\Utilisateur $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \IdvBundle\Entity\Utilisateur 
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Demande
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }


    /**
     * Set verouiller
     *
     * @param integer $verouiller
     * @return Demande
     */
    public function setVerouiller($verouiller)
    {
        $this->verouiller = $verouiller;
    
        return $this;
    }

    /**
     * Get verouiller
     *
     * @return integer 
     */
    public function getVerouiller()
    {
        return $this->verouiller;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->demandeImages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add demandeImages
     *
     * @param \IdvBundle\Entity\Image $demandeImages
     * @return Demande
     */
    public function addDemandeImage(\IdvBundle\Entity\Image $demandeImages)
    {
        $this->demandeImages[] = $demandeImages;

        return $this;
    }

    /**
     * Remove demandeImages
     *
     * @param \IdvBundle\Entity\Image $demandeImages
     */
    public function removeDemandeImage(\IdvBundle\Entity\Image $demandeImages)
    {
        $this->demandeImages->removeElement($demandeImages);
    }

    /**
     * Get demandeImages
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDemandeImages()
    {
        return $this->demandeImages;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Demande
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add categories
     *
     * @param \IdvBundle\Entity\Categorie $categories
     * @return Demande
     */
    public function addCategory(\IdvBundle\Entity\Categorie $categories)
    {
        $this->categories[] = $categories;

        return $this;
    }

    /**
     * Remove categories
     *
     * @param \IdvBundle\Entity\Categorie $categories
     */
    public function removeCategory(\IdvBundle\Entity\Categorie $categories)
    {
        $this->categories->removeElement($categories);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategories()
    {
        return $this->categories;
    }
}
