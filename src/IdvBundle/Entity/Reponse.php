<?php

namespace IdvBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reponse
 *
 * @ORM\Table(name="reponse")
 * @ORM\Entity(repositoryClass="IdvBundle\Repository\ReponseRepository")
 */
class Reponse
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reponse", type="string", length=255, nullable=true)
     */
    private $reponse;

    /**
     * @ORM\ManyToOne(targetEntity="IdvBundle\Entity\Image", inversedBy="reponses")
     * @ORM\JoinColumn(name="demande_id", referencedColumnName="id")
     */
    private $image;

    /**
     * @ORM\ManyToOne(targetEntity="Requester", inversedBy="reponses")
     * @ORM\JoinColumn(name="requester_id", referencedColumnName="id")
     */
    private $requester;

    /**
     * @ORM\ManyToOne(targetEntity="IdvBundle\Entity\Categorie", inversedBy="reponses")
     * @ORM\JoinColumn(name="categorie_id", referencedColumnName="id")
     */
    private $categorie;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reponse
     *
     * @param string $reponse
     * @return Reponse
     */
    public function setReponse($reponse)
    {
        $this->reponse = $reponse;

        return $this;
    }

    /**
     * Get reponse
     *
     * @return string 
     */
    public function getReponse()
    {
        return $this->reponse;
    }

    /**
     * Set requester
     *
     * @param \IdvBundle\Entity\Requester $requester
     * @return Reponse
     */
    public function setRequester(\IdvBundle\Entity\Requester $requester = null)
    {
        $this->requester = $requester;

        return $this;
    }

    /**
     * Get requester
     *
     * @return \IdvBundle\Entity\Requester 
     */
    public function getRequester()
    {
        return $this->requester;
    }

    /**
     * Set categorie
     *
     * @param \IdvBundle\Entity\Categorie $categorie
     * @return Reponse
     */
    public function setCategorie(\IdvBundle\Entity\Categorie $categorie = null)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return \IdvBundle\Entity\Categorie 
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set image
     *
     * @param \IdvBundle\Entity\Image $image
     * @return Reponse
     */
    public function setImage(\IdvBundle\Entity\Image $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \IdvBundle\Entity\Image 
     */
    public function getImage()
    {
        return $this->image;
    }
}
