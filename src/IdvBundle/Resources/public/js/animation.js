
function handleAnnotation(){

    var mouseX = 0, mouseY = 0;
    var dragging = false;
    var tagHolder = $(".tagHolder"),
        box = tagHolder.find(".box");

    $(".btn-annuler").click(function(e){
        e.preventDefault();

        $(".tagHolder").fadeOut();
        $("input[name=xpoint]").val("");
        $("input[name=ypoint]").val("");
        $(".tagHolder").fadeOut();
    });
    $(".tagView").each(function (index) {
        var newLeft = $(this).position().left - ($(this).find(".box").offset().left - $(this).offset().left);
        $(this).css({left: newLeft});
    });
    $(document).on('mousedown', ".imgCanvas", function (e) {
        if (e.which != 1 || dragging)
            return;
        dragging = true;
        mouseX = e.pageX - $(this).offset().left;
        mouseY = e.pageY - $(this).offset().top;
        box.css({width: 0, height: 0});
        tagHolder.css({top: mouseY, left: mouseX, display: "inline"});
    });
    $(document).on('mousemove', ".imgCanvas", function (e) {
        if (!dragging)
            return;
        var newLeft = e.pageX - $(this).offset().left,
            newTop = e.pageY - $(this).offset().top;
        box.css({width: Math.abs(newLeft - mouseX), height: Math.abs(newTop - mouseY)});
        if (newLeft > mouseX)
            newLeft = mouseX;
        if (newTop > mouseY)
            newTop = mouseY;
        tagHolder.css({left: newLeft, top: newTop});
    });
    $(document).on('mouseup', ".imgCanvas", function (e) {
        if (e.which != 1)
            return;
        dragging = false;
        $("input[name=xpoint]").val(mouseX);
        $("input[name=ypoint]").val(mouseY);
        $("input[name=height]").val(box.height());
        $("input[name=width]").val(box.width());
    });

    $('.imgCanvas .tagView').css({opacity: 1});

    // $('.imgCanvas').on('mouseover', '.tagView', function () {
    //     $(this).css({opacity: 1});
    // }).on('mouseout', '.tagView', function () {
    //     $(this).css({opacity: 0});
    // });
}

$(document).ready(function(){

    handleAnnotation()


    $('.login_crw').click(function(){

        $('.login_crw').animate({
            marginLeft: "312px"
        }, 1000, function(){
            $('.frm_crw').css("display","block");
            $('.tmp_login').css("display","none");
            $('.login_crw').css({"background-image":"none", /*"background-color":"#ededed",*/ "border-radius":"10px", "border": "solid 1px #dedede"});
        });

        $('.login_req').fadeOut(0);
    });

    $('.login_req').click(function(){

        $('.login_req').animate({
            marginRight: "312px"
        }, 1000, function(){
            $('.login_req div').css("display","block");
            $('.tmp_login').css("display","none");
            $('.login_req').css({backgroundImage:"none", /*backgroundColor:"#ededed",*/ borderRadius:"10px", border: "solid 1px #dedede"}, 1000);
        });

        $('.login_crw').fadeOut(0);
    });
    /*
    $('.edit_profile').submit(function(e){
        e.preventDefault();


        var $form = $(".edit_profile");
        var formdata = (window.FormData) ? new FormData($form[0]) : null; //console.log(formdata);
        var sdata = (formdata !== null) ? formdata : $form.serialize(); console.log(sdata);

        $.ajax({
            url:$form.attr("action"),
            type:$form.attr("method"),
            data: sdata,
            contentType:false,
            processData:false,
            success: function(rdata, statusText, xhr){
                if(xhr.status == 200)
                    alert("Modification réussite");

            },
            statusCode: {
                402 : alert("mot de passe incorrect")
            }

        });
    });*/

    $('.tab-url').click(function(e){
        e.preventDefault();
        var targetTab = '.'+$(this).data('tab');
        $('.tab-url').removeClass("active");
        $(this).addClass("active");
        $('.target-tab').css("display","none");
        $(targetTab).css("display", "block");
    });
    //show annotation
    $("a[data-toggle=modal]").click(function(){

        var id = $(this).find("input").val();
        // home url
        var homeUrl = Routing.generate('home');
        $.ajax({
            url: Routing.generate("crowder_annotation_show_id", {ida: id}),
            dataType: "json",
            type: "GET",
            success: function(rdata){
                //console.log(rdata);
                $(".img-ann").attr("src",  homeUrl.slice(0,-1) + rdata['url'])
                $(".txt-ann").text(rdata['description']);
                $(".tagView").css({"left":rdata['xPoint']+"px", "top":rdata['yPoint']+"px"});
                $(".box").css({"margin":"0 auto","width":rdata['width']+"px", "height":rdata['height']+"px"});
                $(".td1").text(rdata['titre']);
                $(".td2").text(rdata['confiance']);
                $(".td3").text(rdata['description']);

            }
        })
    });

    $(".lock").change(function(){
        var sdata;
        var parent = $(this).parent();
        var ilock = parent.find($("input[name=ilock]")).val();
        var id = parent.find($("input[name=id]")).val();
        if(ilock == 1)
            sdata = {"lock":"0", "id": id};
        else
            sdata = {"lock":"1", "id": id};

        $.ajax({
            url: "/requester/request/edit",
            data : sdata,
            type: "post",
            success: function(rdata, xhr){
                alert(sdata['lock']);
                parent.find($("input[name=ilock]")).val(sdata['lock']);
                alert("Elément modifié avec succée")

            }
        })
    });

    $(document).on('click', '.validate-analyse', function (event) {
        checkValidateDateEvent($(this), event);
    });
    $(document).on('click', '.refuse-analyse', function (event) {
        checkValidateDateEvent($(this), event);
    });
    var checkValidateDateEvent = function ($element, event) {
        var sDateEcheance = $element.data('echeance');
        var oEcheanceDate = new Date(sDateEcheance);
        var currentDate = new Date();
        if(oEcheanceDate > currentDate) {
            event.preventDefault();
            $.confirm({
                title: 'Attention !',
                content: 'Vous ne pouvez pas clôturer ce projet car la date d\'échéance n\'est pas encore passée.',
                type: 'red',
                closeIcon: true,
                typeAnimated: true,
                buttons: {
                    ok: function () {
                    }
                }
            });

            return false;
        }
    };
    $('[data-toggle="tooltip"]').tooltip();

});

