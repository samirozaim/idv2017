<?php

namespace IdvBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface;

class IdvExtension extends \Twig_Extension
{
	protected $container;


	public function getFunctions()
	{
		return array(
			new \Twig_SimpleFunction('twig_function', array($this, 'twigFunc')),
			new \Twig_SimpleFunction('app_parameter', array($this, 'appParameter')),
			new \Twig_SimpleFunction('get_images_by_demande', array($this->get('annotation_helper'), 'getImagesByDemande')),
			new \Twig_SimpleFunction('is_segmented_group', array($this->get('annotation_helper'), 'isSegmentedGroup')),
			new \Twig_SimpleFunction('is_segmented_group_requester', array($this->get('annotation_helper'), 'isSegmentedGroupRequester')),
			new \Twig_SimpleFunction('is_segmented_image', array($this->get('annotation_helper'), 'isSegmentedImage')),
			new \Twig_SimpleFunction('is_annotated_image', array($this->get('annotation_helper'), 'isAnnotatedImage')),
			new \Twig_SimpleFunction('is_segmented_image_requester', array($this->get('annotation_helper'), 'isSegmentedImageRequester')),
			new \Twig_SimpleFunction('is_annotated_image_requester', array($this->get('annotation_helper'), 'isAnnotatedImageRequester')),
			new \Twig_SimpleFunction('calculate_reponse', array($this->get('annotation_helper'), 'calculateReponse')),
			new \Twig_SimpleFunction('calculate_reponse_biologiste', array($this->get('annotation_helper'), 'calculateReponseBiologiste')),
			new \Twig_SimpleFunction('calculate_segmentation_tag', array($this->get('annotation_helper'), 'calculateAvgSegmentationImage')),
			new \Twig_SimpleFunction('biologiste_reponse_choices', array($this->get('annotation_helper'), 'getBiologisteResponseChoices')),
			new \Twig_SimpleFunction('translate_biologiste_reponse_choice', array($this->get('annotation_helper'), 'translateChoiceBiologiste')),
			new \Twig_SimpleFunction('translate_demande_type', array($this->get('annotation_helper'), 'translateDemandeType')),
		);
	}



	public function __construct(ContainerInterface $container)
	{
		$this->container = $container;
	}

	private function get($serviceName){
		return $this->container->get($serviceName);
	}


	public function twigFunc()
	{
		return "Ceci est une fonction twig, mais c'est la seule qui doit être definie directement dans l'extension Twig. Toutes les autres doivent simplement servir à appeler des methodes de services, helpers, repositories...";
	}

	public function getName()
	{
		return 'idv_extension';
	}

	public function appParameter($parameterName)
	{
		return $this->container->getParameter($parameterName);
	}

}