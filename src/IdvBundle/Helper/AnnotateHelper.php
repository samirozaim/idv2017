<?php

namespace IdvBundle\Helper;

use IdvBundle\Entity\Categorie;
use IdvBundle\Entity\Demande;
use IdvBundle\Entity\Group;
use IdvBundle\Entity\Image;
use IdvBundle\Entity\Reponse;
use IdvBundle\Models\HelperModel;

class AnnotateHelper extends HelperModel
{
    /**
     * Delete old annotation by user and image
     * @param Image $image
     */
    public function resetOldAnnotationByImage(Image $image)
    {
        $annotations  = $this->getRepository('IdvBundle:Annotation')->findBy(['requester' => $this->getCurrentCrowder(), 'imageAnnotation' => $image]);
        foreach ($annotations as $annotation) {
            $this->remove($annotation);
        }
    }

    public function getNextImage(Image $image)
    {
        /** @var Demande $demande */
        $images = $image->getDemande()->getDemandeImages();
        $found = false;
        foreach ($images as $existingImage) {
            if($found) return $existingImage;
            if($image == $existingImage) $found = true;
        }
        return false;
    }

    public function getNextImageGroup(Image $image)
    {
        $groups = $this->getRepository('IdvBundle:Group')->findByDemande($image->getDemande());
        /** @var Group $group */
        foreach ($groups as $group) {
            if(!$this->isSegmentedGroup($group)) {
                return $group->getImages()->first();
            }
        }
        return false;
    }

    public function getPreviewsImage(Image $image)
    {
        /** @var Demande $demande */
        $images = $image->getDemande()->getDemandeImages();
        $previewsImage = false;
        foreach ($images as $existingImage) {
            if($image == $existingImage) return $previewsImage;
            $previewsImage = $existingImage;
        }

        return false;
    }

    public function resetOldReponses(Image $image)
    {
        $reponses = $this->getRepository('IdvBundle:Reponse')->findBy(['requester' => $this->getCurrentCrowder(), 'image' => $image]);
        foreach ($reponses as $reponse) {
            $this->remove($reponse);
        }
    }

    public function createReponseFromStringAndImage(Image $image, Categorie $categorie, $sReponse)
    {
        $crowder = $this->getCurrentCrowder();
        $reponse = $this->getRepository('IdvBundle:Reponse')->findOneBy(['image' => $image, 'requester' => $crowder, 'categorie' => $categorie]);
        if(!$reponse) $reponse = new Reponse();
        $reponse->setImage($image);
        $reponse->setReponse($sReponse);
        $reponse->setRequester($crowder);
        $reponse->setCategorie($categorie);
        $this->persist($reponse);
    }

    public function createChoiceReponseFromStringAndImage(Image $image, $sReponse)
    {
        $crowder = $this->getCurrentCrowder();
        $reponse = $this->getRepository('IdvBundle:Reponse')->findOneBy(['image' => $image, 'requester' => $crowder]);
        if(!$reponse) $reponse = new Reponse();
        $reponse->setImage($image);
        $reponse->setReponse($sReponse);
        $reponse->setRequester($crowder);
        $this->persist($reponse);
    }

    public function getImagesByDemande(Demande $demande)
    {
    	$images = $demande->getDemandeImages();
    	$groupImages = [];
    	/** @var Image $image */
	    foreach ( $images as $image ) {
		    $groupImages[$image->getGroup()->getId()]['images'][] = $image;
		    $groupImages[$image->getGroup()->getId()]['group'] = $image->getGroup();
    	}

    	return $groupImages;
    }

    public function isSegmentedGroup(Group $group)
    {
        $currentUser = $this->getCurrentCrowder();
        $images = $group->getImages();
        $segmented = false;
        /** @var Image $image */
        foreach ($images as $image) {
            $annotation = $this->getRepository('IdvBundle:Annotation')->findOneBy(['requester' => $currentUser, 'imageAnnotation' => $image]);
            if($annotation) $segmented = true;
        }
        return $segmented;
    }

    public function isSegmentedImage(Image $image)
    {
        $currentUser = $this->getCurrentCrowder();
        $annotation = $this->getRepository('IdvBundle:Annotation')->findOneBy(['requester' => $currentUser, 'imageAnnotation' => $image]);
        return ($annotation)? true: false;
    }
    public function isSegmentedGroupRequester(Group $group)
    {
        $images = $group->getImages();
        $segmented = false;
        /** @var Image $image */
        foreach ($images as $image) {
            $annotation = $this->getRepository('IdvBundle:Annotation')->findOneBy(['imageAnnotation' => $image]);
            if($annotation) $segmented = true;
        }
        return $segmented;
    }

    public function isSegmentedImageRequester(Image $image)
    {
        $annotation = $this->getRepository('IdvBundle:Annotation')->findOneBy(['imageAnnotation' => $image]);
        return ($annotation)? true: false;
    }

    public function isAnnotatedImage(Image $image)
    {
        $currentUser = $this->getCurrentCrowder();
        $reponse = $this->getRepository('IdvBundle:Reponse')->findOneBy(['requester' => $currentUser, 'image' => $image]);
        return ($reponse)? true: false;
    }

    public function isAnnotatedImageRequester(Image $image)
    {
        $reponse = $this->getRepository('IdvBundle:Reponse')->findOneBy(['image' => $image]);
        return ($reponse)? true: false;
    }

    public function calculateReponse(Image $image, Categorie $categorie)
    {
        $mostReponse = $this->getRepository('IdvBundle:Reponse')->getMostReponseByCategoryAndImage($image, $categorie);
        if(empty($mostReponse)) return '';
        return $mostReponse[0]['reponse'];
    }

    public function calculateReponseBiologiste(Image $image)
    {
        $mostReponse = $this->getRepository('IdvBundle:Reponse')->calculateAVGByImage($image);
        if(empty($mostReponse)) return '';
        return $mostReponse[0]['reponse'];
    }

    public function calculateAvgSegmentationImage(Image $image)
    {
        $avgAnnotations = $this->getRepository('IdvBundle:Annotation')->getAvgSegmentationImage($image);
        return $avgAnnotations;
    }

    /**
     * Return array list of choices
     */
    public function getBiologisteResponseChoices()
    {
        $choices = [];
        $choices[]= [ 'value' => '10', 'label' => '10%' ];
        $choices[]= [ 'value' => '20', 'label' => '20%' ];
        $choices[]= [ 'value' => '30', 'label' => '30%' ];
        $choices[]= [ 'value' => '40', 'label' => '40%' ];
        $choices[]= [ 'value' => '50', 'label' => '50%' ];
        $choices[]= [ 'value' => '60', 'label' => '60%' ];
        $choices[]= [ 'value' => '70', 'label' => '70%' ];
        $choices[]= [ 'value' => '80', 'label' => '80%' ];
        $choices[]= [ 'value' => '90', 'label' => '90%' ];
        $choices[]= [ 'value' => '100', 'label' => '100%' ];

        return $choices;
    }

    /**
     * Return array list of choices
     */
    public function translateChoiceBiologiste($value)
    {
        $choices = $this->getBiologisteResponseChoices();
        foreach ($choices as $choice) {
            if($choice['value'] == $value) return $choice['label'];
        }

        return '';
    }

    public function translateDemandeType($string)
    {
        $aTypes = [
            'biologiste' => 'Qualité Image'
        ];
        return (isset($aTypes[$string]))? $aTypes[$string]: $string;
    }
}